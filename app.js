const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes')


const app = express();
const port = 4000;


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/* Syntax:
mongoose.connect('<connection string>',{middleware}) */

mongoose.connect('mongodb+srv://admin:admin@cluster0.3zkqs.mongodb.net/session30?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true

    /* to avoid futures error when connecting mongodb */
});

let db = mongoose.connection;

/* console.error.bind(console) - print errors in the browser and in the terminal. */
db.on("error", console.error.bind(console, "Connection Error"));
/* if the connection is successful, this will be the output in our console. */
db.once('open', () => console.log('Connected to the cloud database.'));


/* Mongoose Schema Section */

app.use('/tasks', taskRoutes)

/* port listener */
app.listen(port, () => console.log(`Server is running at localhost ${port}`));