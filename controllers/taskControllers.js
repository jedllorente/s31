const Task = require('../models/taskSchema')

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result
    })
}

// Creating task
module.exports.createTask = (reqBody) => {
    let newTask = new Task({
        name: reqBody.name
    })
    return newTask.save().then((task, err) => {
        if (err) {
            console.log(err)
        } else {
            return task
        }
    })
}

// delete Task

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndDelete(taskId).then((removedTask, err) => {
        if (err) {
            console.log(err)
            return false
        } else {
            return removedTask
        }
    })
}

// update task

module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, err) => {
        if (err) {
            console.log(err)
            return false
        }
        result.name = newContent.name
        return result.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr)
                return false
            } else {
                return updatedTask
            }
        })
    })
}

// retrieve specificTask

module.exports.retrieveOneTask = (taskId) => {
    return Task.findById(taskId).then(result => {
        return result
    })
}

// complete status

module.exports.completeTask = (taskId) => {
    return Task.findById(taskId).then((result, err) => {
        if (err) {
            console.log(err)
            return false
        }
        result.status = 'complete'
        return result.save().then((completedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr)
                return false
            } else {
                return completedTask
            }
        })
    })
}