const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskControllers')

// route for displaying tasks
router.get('/', (req, res) => {
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

// route for creating Task
router.post('/createTask', (req, res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// route for deleting task
router.delete('/deleteTask/:id', (req, res) => {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// route for updating task
router.put('/updateTask/:id', (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// retrieve specific task
router.get('/:id', (req, res) => {
    taskController.retrieveOneTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// route for updating status
router.put('/:id/complete', (req, res) => {
    taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController))
})
module.exports = router